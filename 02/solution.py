#!/usr/bin/env python

def checksum(spreadsheet):
    sum = 0
    for line in spreadsheet.split('\n'):
        if line == '':
            continue
        line = line.strip()
        line = [int(digit) for digit in line.split('\t')]
        sum += max(line) - min(line)
    return sum


def even_checksum(spreadsheet):
    sum = 0
    for line in spreadsheet.split('\n'):
        if line == '':
            continue
        line = line.strip()
        line = [int(digit) for digit in line.split('\t')]
        for digit in line:
            for digit_2 in line:
                if digit_2 >= digit:
                    continue
                elif digit % digit_2 == 0:
                    sum += int(digit / digit_2)
    return sum


def main():
    with open('input') as input_data:
        print(checksum(input_data.read()))
        input_data.seek(0)
        print(even_checksum(input_data.read()))

if __name__ == '__main__':
    main()
