import pytest

from solution import checksum, even_checksum

spreadsheet = '''5	1	9	5
7	5	3
2	4	6	8
'''
evens = '''5	9	2	8
9	4	7	3
3	8	6	5'''

def test_checksum():
    assert checksum(spreadsheet) == 18

def test_even_checksum():
    assert even_checksum(evens) == 9
