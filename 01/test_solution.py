import pytest

from solution import special_sum, half_sum

def test_special_sum():
    assert special_sum('1122') == 3
    assert special_sum('1111') == 4
    assert special_sum('1234') == 0
    assert special_sum('91212129') == 9


def test_half_sum():
    assert half_sum('1212') == 6
    assert half_sum('1221') == 0
    assert half_sum('123425') == 4
    assert half_sum('123123') == 12
    assert half_sum('12131415') == 4
