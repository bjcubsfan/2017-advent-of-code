import pytest

from solution import repeat_number

def test_repeat_number():
    first_repeat, repeated_state = repeat_number('0 2 7 0')
    assert first_repeat == 5
    full_cycle, repeated_state = repeat_number(
        repeated_state,
        exact_state=tuple(repeated_state),
        )
