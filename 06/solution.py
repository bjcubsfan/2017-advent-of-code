#!/usr/bin/env python

def perform_cycle(current_state):
    biggest = max(current_state)
    remove_from = current_state.index(biggest)
    current_state[remove_from] = 0
    add_here = remove_from
    while biggest:
        add_here = (add_here + 1) % len(current_state)
        current_state[add_here] += 1
        biggest -= 1
    return current_state


def repeat_number(banks, exact_state=None):
    if type(banks) == str:
        current_state = [int(num) for num in banks.split()]
    else:
        current_state = banks
    number_of_cycles = 0
    seen_cycles = set()
    seen_cycles.add(tuple(current_state))
    while True:
        next_state = perform_cycle(current_state)
        number_of_cycles += 1
        if not exact_state:
            quit = (tuple(next_state) in seen_cycles)
        else:
            quit = (tuple(exact_state) == tuple(next_state))
        if quit:
            return number_of_cycles, next_state
        else:
            seen_cycles.add(tuple(next_state))
            current_state = next_state


def main():
    with open('input') as input_data:
        for line in input_data:
            num, state = repeat_number(line)
            print(num)
            num, state = repeat_number(
                state,
                exact_state=tuple(state)
                )
            print(num)


if __name__ == '__main__':
    main()
