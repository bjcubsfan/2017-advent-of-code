import pytest

from solution import steps, adding


@pytest.mark.parametrize(
    'location, actual_steps', [
        (1, 0),
        (2, 1),
        (3, 2),
        (12, 3),
        (23, 2),
        (1024, 31),
        ])
def test_steps(location, actual_steps):
    steps_for_location = steps(location)
    assert steps_for_location == actual_steps


@pytest.mark.parametrize(
    'location, actual_sum', [
        (1, 1),
        (5, 5),
        (6, 10),
        (11, 54),
        (17, 147),
        (21, 362),
        (22, 747),
        (23, 806),
        ])
def test_adding(location, actual_sum):
    sum_for_location = adding(location)
    assert sum_for_location == actual_sum
