#!/usr/bin/env python
"""
Here are the numbers:

17  16  15  14  13
18   5   4   3  12
19   6   1   2  11
20   7   8   9  10
21  22  23---> ...

Here are their coordinates added:

17(-2,2)  16(-1,2)  15(0,2)  14(1,2)  13(2,2)
18(-2,1)   5(-1,1)   4(0,1)   3(1,1)  12(2,1)
19(-2,0)   6(-1,0)   1(0,0)   2(1,0)  11(2,0)
20(-2,-1)  7(-1,-1)  8(0,-1)  9(1,-1) 10(2,-1)
21(-2,-2) 22(-1,-2) 23(0,-2) 24(1,-2) 25(-2,-2)

"""

def next_spiral(previous_coordinates, direction, filled_coordinates):
    # if moving right, turn up when there's a free space, else move right
    if direction == 'R':
        up_space = (previous_coordinates[0], previous_coordinates[1] + 1)
        if up_space not in filled_coordinates:
            direction = 'U'
            previous_coordinates = up_space
        else:
            previous_coordinates = (previous_coordinates[0] + 1, previous_coordinates[1])
        filled_coordinates[previous_coordinates] = 0
    # if moving up, turn left when there's a free space, else move up
    elif direction == 'U':
        left_space = (previous_coordinates[0] - 1, previous_coordinates[1])
        if left_space not in filled_coordinates:
            direction = 'L'
            previous_coordinates = left_space
        else:
            previous_coordinates = (previous_coordinates[0], previous_coordinates[1] + 1)
        filled_coordinates[previous_coordinates] = 0
    # if moving left, turn down when there's a free space, else move left
    elif direction == 'L':
        down_space = (previous_coordinates[0], previous_coordinates[1] - 1)
        if down_space not in filled_coordinates:
            direction = 'D'
            previous_coordinates = down_space
        else:
            previous_coordinates = (previous_coordinates[0] - 1, previous_coordinates[1])
        filled_coordinates[previous_coordinates] = 0
    # if moving down, turn right when there's a free space, else move down
    elif direction == 'D':
        right_space = (previous_coordinates[0] + 1, previous_coordinates[1])
        if right_space not in filled_coordinates:
            direction = 'R'
            previous_coordinates = right_space
        else:
            previous_coordinates = (previous_coordinates[0], previous_coordinates[1] - 1)
        filled_coordinates[previous_coordinates] = 0
    return previous_coordinates, direction, filled_coordinates

def steps(location):
    if location == 1:
        return 0
    previous_coordinates = (1, 0)
    direction = 'R'
    filled_coordinates = dict()
    filled_coordinates[(0, 0)] = 0
    filled_coordinates[(1, 0)] = 0
    for step in range(3, location + 1):
        previous_coordinates, direction, filled_coordinates = next_spiral(
            previous_coordinates,
            direction,
            filled_coordinates,
            )
    return abs(previous_coordinates[0]) + abs(previous_coordinates[1])


def sum_for_coordinates(previous_coordinates, filled_coordinates):
    add_to_check = (
        (0, 1),
        (1, 1),
        (1, 0),
        (-1, 0),
        (0, -1),
        (-1, -1),
        (-1, 1),
        (1, -1),
        )
    sum = 0
    for neighbor in add_to_check:
        to_check = (
            previous_coordinates[0] + neighbor[0],
            previous_coordinates[1] + neighbor[1],
            )
        if to_check in filled_coordinates:
            sum += filled_coordinates[to_check]
    return sum


def adding(location, stop_if_greater_than=None):
    if location == 1 or location == 2:
        return 1
    previous_coordinates = (1, 0)
    direction = 'R'
    filled_coordinates = dict()
    filled_coordinates[(0, 0)] = 1
    filled_coordinates[(1, 0)] = 1
    for step in range(3, location + 1):
        previous_coordinates, direction, filled_coordinates = next_spiral(
            previous_coordinates,
            direction,
            filled_coordinates,
            )
        this_sum = sum_for_coordinates(
            previous_coordinates,
            filled_coordinates,
            )
        print(this_sum)
        if stop_if_greater_than and this_sum > stop_if_greater_than:
            return this_sum
        filled_coordinates[previous_coordinates] = this_sum
    return filled_coordinates[previous_coordinates]


def main():
    print(steps(277678))
    print(adding(2000, stop_if_greater_than=277678))


if __name__ == '__main__':
    main()
