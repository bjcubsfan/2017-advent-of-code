import pytest

from solution import escape_number

instructions = """ 0
3
0
1
-3
"""

def test_escape_number():
    assert escape_number(instructions) == 5

def test_escape_number_weirder():
    assert escape_number(instructions, weirder=True) == 10
