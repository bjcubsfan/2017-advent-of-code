#!/usr/bin/env python

def escape_number(instruction_input, weirder=False):
    instructions = []
    for line in instruction_input.strip().split('\n'):
        try:
            instructions.append(int(line))
        except ValueError:
            import IPython; IPython.embed()
    within_instructions = True
    step = 0
    index = 0
    while within_instructions:
        if index >= len(instructions) or index < 0:
            within_instructions = False
        else:
            jump = instructions[index]
            if weirder and jump >= 3:
                instructions[index] -= 1
            else:
                instructions[index] += 1
            index = index + jump
            step += 1
    return step

def main():
    with open('input') as input_data:
        print(escape_number(input_data.read()))
    with open('input') as input_data:
        print(escape_number(input_data.read(), weirder=True))

if __name__ == '__main__':
    main()
