import pytest

from solution import valid, extra_valid


# @pytest.mark.parametrize(
#     'passphrase', 'given_validity', [
#         ('aa bb cc dd ee', True),
#         ('aa bb cc dd aa', False),
#         ('aa bb cc dd aaa', True),
#         ])
def test_valid():
    assert True == valid('aa bb cc dd ee')
    assert False == valid('aa bb cc dd aa')
    assert True == valid('aa bb cc dd aaa')

def test_extra_valid():
    assert extra_valid('abcde fghij') == True
    assert extra_valid('abcde xyz ecdab') == False
    assert extra_valid('a ab abc abd abf abj') == True
    assert extra_valid('iiii oiii ooii oooi oooo') == True
    assert extra_valid('oiii ioii iioi iiio') == False
