#!/usr/bin/env python

from itertools import permutations

def valid(passphrase):
    words = set()
    for word in passphrase.split():
        if word in words:
            return False
        else:
            words.add(word)
    return True

def extra_valid(passphrase):
    if not valid(passphrase):
        return False
    words = [word for word in passphrase.split()]
    for word in words:
        for mix in permutations(word, len(word)):
            mix = ''.join(mix)
            if mix == word:
                continue
            elif mix in words:
                return False
    return True


def main():
    number_valid = 0
    number_extra_valid = 0
    with open('input') as input_data:
        for passphrase in input_data:
            if valid(passphrase):
                number_valid += 1
            if extra_valid(passphrase):
                number_extra_valid += 1
    print(number_valid)
    print(number_extra_valid)

if __name__ == '__main__':
    main()
